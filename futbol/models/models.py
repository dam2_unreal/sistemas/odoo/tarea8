# -*- coding: utf-8 -*-

from odoo import models, fields, api

class futbolistas(models.Model):
	_name = 'futbol.futbolistas'

	Nombre = fields.Char(string='Nombre')
	Equipo = fields.Char(string='Equipo')
	Edad = fields.Integer(string='Edad')

class equipos(models.Model):
	_name = 'futbol.equipos'

	Nombre = fields.Char(string='Nombre')
	Fundacion = fields.Integer(string='Fundacion')
	Ligas = fields.Integer(string='Ligas')


